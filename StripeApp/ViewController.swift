//
//  ViewController.swift
//  StripeApp
//
//  Created by Rana AITS on 12/7/20.
//

import UIKit
import Stripe



class ViewController: UIViewController, STPPaymentContextDelegate  {
    
    let customerContext = STPCustomerContext(keyProvider: MyAPIClient())
    let paymentContext : STPPaymentContext
    

    
    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
        self.paymentContext = STPPaymentContext(customerContext: customerContext)
        super.init(nibName: nil, bundle: nil)
        self.paymentContext.delegate = self
        self.paymentContext.hostViewController = self
        self.paymentContext.paymentAmount = 5000 // This is
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let button = UIButton(frame: CGRect(x: 20, y: 20, width: 200, height: 60))
        button.setTitle("Email", for: .normal)
        button.backgroundColor = .white
        button.setTitleColor(UIColor.black, for: .normal)
        button.addTarget(self, action: #selector(self.buttonTapped), for: .touchUpInside)
        view.addSubview(button)
        
        // Do any additional setup after loading the view.
        
        
    }
    @objc func buttonTapped(sender : UIButton) {
        //Write button action here
        choosePaymentButtonTapped()
    }
    
    
    
    func choosePaymentButtonTapped() {
        self.paymentContext.presentPaymentOptionsViewController()
    }
    
    
    func paymentContextDidChange(_ paymentContext: STPPaymentContext) {
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFailToLoadWithError error: Error) {
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didCreatePaymentResult paymentResult: STPPaymentResult, completion: @escaping STPPaymentStatusBlock) {
        
    }
    
    func paymentContext(_ paymentContext: STPPaymentContext, didFinishWith status: STPPaymentStatus, error: Error?) {
        
    }
    
    
    
    //
    
}

